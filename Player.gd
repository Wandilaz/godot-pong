extends StaticBody2D
class_name Player

const SCENE_FILENAME = "res://Player.tscn"

export var speed := 200
var viewport_height: float
var up_action: String
var down_action: String


static func instance() -> Player:
	return load(SCENE_FILENAME).instance()

func _ready() -> void:
	viewport_height = get_viewport_rect().size.y


func _process(delta) -> void:
	if Input.is_action_pressed(up_action):
		position.y -= speed * delta
	if Input.is_action_pressed(down_action):
		position.y += speed * delta
	
	var vertical_extent := \
			($CollisionShape2D.shape as RectangleShape2D).extents.y
	position.y = clamp(
		position.y,
		vertical_extent,
		viewport_height - vertical_extent
	)


func set_input_actions(up_a: String, down_a: String) -> void:
	up_action = up_a
	down_action = down_a
