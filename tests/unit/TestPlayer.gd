extends "res://addons/gut/test.gd"

const VIEWPORT_SIZE = Vector2(100, 600)
const START_POSITION = Vector2(50, 300)
const PLAYER_VERTICAL_EXTENT = 25.0
const PLAYER_SPEED = 200
const PLAYER_UP_ACTION = "p1_up"
const PLAYER_DOWN_ACTION = "p1_down"

var player : Player


func before_all() -> void:
	get_tree().set_screen_stretch(
		SceneTree.STRETCH_MODE_2D,
		SceneTree.STRETCH_ASPECT_KEEP,
		VIEWPORT_SIZE
	)


func before_each() -> void:
	player = Player.instance()
	player.set_input_actions(PLAYER_UP_ACTION, PLAYER_DOWN_ACTION)
	player.position = START_POSITION
	
	add_child_autoqfree(player)


func test_move_up() -> void:
	simulate_action(PLAYER_UP_ACTION)
	gut.simulate(player, 10, .1)
	simulate_action(PLAYER_UP_ACTION, true)
	
	assert_eq(player.position.y, START_POSITION.y - PLAYER_SPEED)


func test_move_down() -> void:
	simulate_action(PLAYER_DOWN_ACTION)
	gut.simulate(player, 10, .1)
	simulate_action(PLAYER_DOWN_ACTION, true)
	
	assert_eq(player.position.y, START_POSITION.y + PLAYER_SPEED)


func test_clamp_up() -> void:
	simulate_action(PLAYER_UP_ACTION)
	gut.simulate(player, 5, 1)
	simulate_action(PLAYER_UP_ACTION, true)
	
	assert_eq(player.position.y, PLAYER_VERTICAL_EXTENT)


func test_clamp_down() -> void:
	simulate_action(PLAYER_DOWN_ACTION)
	gut.simulate(player, 5, 1)
	simulate_action(PLAYER_DOWN_ACTION, true)
	
	assert_eq(player.position.y, VIEWPORT_SIZE.y - PLAYER_VERTICAL_EXTENT)


func simulate_action(action: String, release: bool = false) -> void:
	var ev = InputEventAction.new()
	ev.action = action
	ev.pressed = !release
	Input.parse_input_event(ev)
