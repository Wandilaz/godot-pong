extends "res://addons/gut/test.gd"

const Main = preload("res://Main.tscn")

const PLAYER_SCENE_FILENAME = "res://Player.tscn"
const START_POSITION_P1 = Vector2(50, 192)
const START_POSITION_P2 = Vector2(462, 192)

var main : Node
var p1 : Player
var p2 : Player


func before_each() -> void:
	main = add_child_autoqfree(Main.instance())
	
	for p in get_children_from_filename(main, PLAYER_SCENE_FILENAME):
		if p.position == START_POSITION_P1 && p1 == null:
			p1 = p
		elif p.position == START_POSITION_P2:
			p2 = p


func test_players_initialized() -> void:
	assert_true(
		get_children_from_filename(main, PLAYER_SCENE_FILENAME).size() == 2
	)


func test_players_good_start_position() -> void:
	assert_true(p1 && p2)


func get_children_from_filename(node: Node, filename: String) -> Array:
	var returned_nodes = []
	
	for i in node.get_children():
		if i.get_filename() == filename:
			returned_nodes.append(i)
		
		returned_nodes += get_children_from_filename(i, filename)
	
	return returned_nodes
