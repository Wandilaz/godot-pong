extends Node

const P1_UP_ACTION = "p1_up"
const P1_DOWN_ACTION = "p1_down"
const P2_UP_ACTION = "p2_up"
const P2_DOWN_ACTION = "p2_down"


func _ready() -> void:
	new_match()


func new_match() -> void:
	var player_1 := Player.instance()
	var player_2 := Player.instance()
	
	player_1.set_input_actions(P1_UP_ACTION, P1_DOWN_ACTION)
	player_2.set_input_actions(P2_UP_ACTION, P2_DOWN_ACTION)
	
	player_1.position = $StartPositionP1.position
	player_2.position = $StartPositionP2.position
	
	add_child(player_1)
	add_child(player_2)
